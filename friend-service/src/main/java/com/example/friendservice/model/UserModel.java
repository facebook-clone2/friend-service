package com.example.friendservice.model;

import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class UserModel {
  private int id;

  private int senderUserId;

  private String name;

  private String imageUrl;

  private int publicProfile;
}
