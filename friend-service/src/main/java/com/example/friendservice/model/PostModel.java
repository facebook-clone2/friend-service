package com.example.friendservice.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@AllArgsConstructor
@Slf4j
@NoArgsConstructor
@Getter
@Setter
public class PostModel {
  private Integer userId;
  private String name;
  private String text;

  private String contentType;

  private String contentUrl;
  private Boolean isStory;
}
