package com.example.friendservice.repository;

import com.example.friendservice.entity.FriendRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
@Repository
public interface FriendRequestRepository extends JpaRepository<FriendRequest, Long> {



    Optional<FriendRequest> findBySenderUserIdAndReceiverUserId(Integer senderUserId, Integer receiverUserId);

    List<FriendRequest> findByReceiverUserIdAndStatus(Integer receiverUserId,String pending);
}
