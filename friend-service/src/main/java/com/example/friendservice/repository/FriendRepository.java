package com.example.friendservice.repository;

import com.example.friendservice.entity.Friend;
import org.springframework.data.neo4j.repository.Neo4jRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
@Repository
public interface FriendRepository extends Neo4jRepository<Friend, Long> {
    Optional<Friend> findByUserId(Integer userId);
}
