// package com.example.friendservice.service;
//
// import com.example.friendservice.entity.Friend;
// import com.example.friendservice.model.FriendsModel;
// import com.example.friendservice.model.PostModel;
// import com.example.friendservice.repository.FriendRepository;
// import lombok.extern.slf4j.Slf4j;
// import org.springframework.beans.factory.annotation.Autowired;
// import org.springframework.stereotype.Service;
// import org.springframework.transaction.annotation.Transactional;
// import org.springframework.web.client.RestTemplate;
//
// import java.util.ArrayList;
// import java.util.List;
// import java.util.Objects;
//
// @Slf4j
//
// @Service
// public class FriendService {
//
//    @Autowired
//    private FriendRepository friendRepository;
//
//    @Autowired
//    private RestTemplate restTemplate;
//
//    @Transactional
//    public boolean addFriend(Integer userId1, Integer userId2) {
//        Friend friend1 = friendRepository.findByUserId(userId1).orElse(null);
//        Friend friend2 = friendRepository.findByUserId(userId2).orElse(null);
//
//        if (friend1 != null && friend2 != null && areFriends(friend1, friend2)) {
//            log.info("Inside if, already friends ");
//            return false;
//        }
//
//
//        if (friend1 == null) {
//            friend1 = new Friend();
//            friend1.setUserId(userId1);
//            friendRepository.save(friend1);
//        }
//
//
//        if (friend2 == null) {
//            friend2 = new Friend();
//            friend2.setUserId(userId2);
//            friendRepository.save(friend2);
//        }
//
//        friend1.getFriends().add(friend2);
//        friend2.getFriends().add(friend1);
//
//        friendRepository.save(friend1);
//        friendRepository.save(friend2);
//
//        return true;
//    }
//
//    private boolean areFriends(Friend friend1, Friend friend2) {
//        log.info("friend1.getFriends(): {}", friend1.getFriends().toString());
//        log.info("friend2.getFriends(): {}", friend2.getFriends().toString());
//
//        boolean areFriends = friend1.getFriends().stream()
//                .anyMatch(friend -> Objects.equals(friend.getUserId(), friend2.getUserId()));
//
//        log.info("Inside areFriends function: {} ", areFriends);
//        return areFriends;
//    }
//
//    @Transactional(readOnly = true)
//    public List<FriendsModel> getFriends(Integer userId) {
//        Friend friend = friendRepository.findByUserId(userId).orElse(null);
//        if (friend != null) {
//            List<Integer> friendIds = new ArrayList<>();
//            friend.getFriends().forEach(f -> friendIds.add(f.getUserId()));
//
//            String userMicroserviceUrl = "http://10.65.1.188:8081/v1.0/users/friendsModel";
//            List<FriendsModel> friendsModel = restTemplate.postForObject(userMicroserviceUrl,
// friendIds, List.class);
//
//            return friendsModel;
//        }
//        return null;
//    }
//
//    public List<PostModel> getPostsOfFriends(int userId){
//        Friend friend = friendRepository.findByUserId(userId).orElse(null);
//        if (friend != null) {
//            List<Integer> friendIds = new ArrayList<>();
//            friend.getFriends().forEach(f -> friendIds.add(f.getUserId()));
//
//            String userMicroserviceUrl = "http://localhost:8081/v1.0/post/getAllPostByUserIds";
//            List<PostModel> postsModel = restTemplate.postForObject(userMicroserviceUrl,
// friendIds, List.class);
//
//            return postsModel;
//        }
//        return null;
//    }
// }
package com.example.friendservice.service;

import com.example.friendservice.entity.Friend;
import com.example.friendservice.entity.FriendRequest;
import com.example.friendservice.model.FriendsModel;
import com.example.friendservice.model.PostModel;
import com.example.friendservice.model.UserModel;
import com.example.friendservice.repository.FriendRepository;
import com.example.friendservice.repository.FriendRequestRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.relational.core.sql.In;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

@Slf4j
@Service
public class FriendService {

  @Autowired private FriendRepository friendRepository;

  @Autowired private FriendRequestRepository friendRequestRepository;

  @Autowired private RestTemplate restTemplate;

  @Transactional
  public String addFriendRequest(Integer userId1, Integer userId2) {
    if(Objects.equals(userId1, userId2)){
      return "Invalid request!";
    }
    if (areFriends(userId1, userId2)) {
      return "Friends Already!";
    }

    int isPublic = restTemplate.postForObject("http://10.65.1.188:8081/v1.0/users/getAccountType", userId2, Integer.class);
    log.info("IsPublic:{}", isPublic);
    if (isPublic == 1) {
      if (addFriend(userId1, userId2)) {
        return "Friends added successfully!";
      } else {
        return "Friends Already!";
      }
    } else {
      if(sendFriendRequest(userId1, userId2))
      return "Friend request sent successfully!";
      else
        return "Friend request already sent";
    }
  }

  @Transactional
  public boolean sendFriendRequest(Integer senderUserId, Integer receiverUserId) {
    FriendRequest existingRequest =
        friendRequestRepository
            .findBySenderUserIdAndReceiverUserId(senderUserId, receiverUserId)
            .orElse(null);

    if (existingRequest == null) {
      FriendRequest friendRequest = new FriendRequest();
      friendRequest.setSenderUserId(senderUserId);
      friendRequest.setReceiverUserId(receiverUserId);
      friendRequest.setStatus("PENDING");
      friendRequest.setTimestamp(System.currentTimeMillis());
      friendRequestRepository.save(friendRequest);
      return true;
    } else {
      log.info("Friend request already sent");
      return false;
    }
  }

  @Transactional(readOnly = true)
  public List<UserModel> getFriendRequestsByReceiver(Integer receiverUserId,String pending) {
    List<FriendRequest> friendRequests = friendRequestRepository.findByReceiverUserIdAndStatus(receiverUserId,"PENDING");
    List<Integer> senderIds = friendRequests.stream()
            .map(FriendRequest::getSenderUserId)
            .collect(Collectors.toList());
    List<UserModel> userModels = new ArrayList<>();
    for(Integer i:senderIds){
      UserModel userModel = restTemplate.postForObject("http://10.65.1.188:8081/v1.0/users/userDetailsForFriendRequests",i,UserModel.class);
      userModel.setSenderUserId(i);
      userModels.add(userModel);
    }return userModels;
  }

  @Transactional
  public boolean approveFriendRequest(Integer senderUserId, Integer receiverUserId) {
    FriendRequest friendRequest =
        friendRequestRepository
            .findBySenderUserIdAndReceiverUserId(senderUserId, receiverUserId)
            .orElse(null);

    if (friendRequest != null && friendRequest.getStatus().equals("PENDING")) {
      friendRequest.setStatus("ACCEPTED");
      friendRequestRepository.save(friendRequest);

      if (addFriend(senderUserId, receiverUserId)) {
        log.info("Friend request approved, and users are now friends");
        return true;
      } else {
        log.error("Error making users friends in the graph");
        return false;
      }
    } else {
      log.error("Invalid friend request or already approved");
      return false;
    }
  }

  public boolean addFriend(Integer userId1, Integer userId2) {
    Friend friend1 = friendRepository.findByUserId(userId1).orElse(null);
    Friend friend2 = friendRepository.findByUserId(userId2).orElse(null);
    if (friend1 == null) {
      friend1 = new Friend();
      friend1.setUserId(userId1);
      friend1 = friendRepository.save(friend1);
    }

    if (friend2 == null) {
      friend2 = new Friend();
      friend2.setUserId(userId2);
      friend2 = friendRepository.save(friend2);
    }

    log.info("Friends for 1 : {}", friend1.getFriends());
    log.info("Friends for 2 : {}", friend2.getFriends());

    friend1.getFriends().add(friend2);
    friendRepository.save(friend1);

    //    friend1 = friendRepository.findByUserId(userId1).orElseThrow();
    friend2 = friendRepository.findByUserId(userId2).orElseThrow();

    friend2.getFriends().add(friend1);
    friendRepository.save(friend2);

    log.info("Friends for 1 : {}", friend1.getFriends());
    log.info("Friends for 2 : {}", friend2.getFriends());
    return true;
  }

  public boolean areFriends(List<Integer> pair){
      int userId1 = pair.get(0);
      int userId2 = pair.get(1);
      return areFriends(userId1,userId2);
  }
  public boolean areFriends(Integer userId1, Integer userId2) {
    Friend friend1 = friendRepository.findByUserId(userId1).orElse(null);
    Friend friend2 = friendRepository.findByUserId(userId2).orElse(null);

    return friend1 != null && friend2 != null && areFriends(friend1, friend2);
  }

  private boolean areFriends(Friend friend1, Friend friend2) {
    log.info("friend1.getFriends(): {}", friend1.getFriends().toString());
    log.info("friend2.getFriends(): {}", friend2.getFriends().toString());

    return friend1.getFriends().stream()
        .anyMatch(friend -> Objects.equals(friend.getUserId(), friend2.getUserId()));
  }

  @Transactional(readOnly = true)
  public List<FriendsModel> getFriends(Integer userId) {
    Friend friend = friendRepository.findByUserId(userId).orElse(null);
    if (friend != null) {
      List<Integer> friendIds = new ArrayList<>();
      friend.getFriends().forEach(f -> friendIds.add(f.getUserId()));
      for (Integer i : friendIds) System.out.println("ID1:"+i);
      String userMicroserviceUrl = "http://10.65.1.188:8081/v1.0/users/friendsModel";
      return restTemplate.postForObject(userMicroserviceUrl, friendIds, List.class);
    }
    return null;
  }

  @Transactional
  public List<PostModel> getPostsOfFriends(int userId) {
    Friend friend = friendRepository.findByUserId(userId).orElse(null);
    if (friend != null) {
      List<Integer> friendIds = new ArrayList<>();
      friend.getFriends().forEach(f -> friendIds.add(f.getUserId()));
      log.info("GOT CALL HERE: {}", friendIds);
      String userMicroserviceUrl = "http://10.65.1.103:8081/v1.0/post/getAllPostByUserIds";
      return restTemplate.postForObject(userMicroserviceUrl, friendIds, List.class);
    }
    return null;
  }

  @Transactional
  public List<PostModel> getStoriesOfFriends(int userId) {
    Friend friend = friendRepository.findByUserId(userId).orElse(null);
    if (friend != null) {
      List<Integer> friendIds = new ArrayList<>();
      friend.getFriends().forEach(f -> friendIds.add(f.getUserId()));

      String userMicroserviceUrl = "http://10.65.1.103:8081/v1.0/post/getAllStoryByUserIds";
      return restTemplate.postForObject(userMicroserviceUrl, friendIds, List.class);
    }
    return null;
  }

  @Transactional
  public boolean declineFriendRequest(Integer senderUserId, Integer receiverUserId) {
    FriendRequest friendRequest =
        friendRequestRepository
            .findBySenderUserIdAndReceiverUserId(senderUserId, receiverUserId)
            .orElse(null);

    if (friendRequest != null && friendRequest.getStatus().equals("PENDING")) {
      friendRequest.setStatus("DECLINED");
      friendRequestRepository.save(friendRequest);
      log.info("Friend request declined successfully");
      return true;
    } else {
      log.error("Invalid friend request or already declined/approved");
      return false;
    }
  }

  @Transactional
  public List<Integer> getFriendsList(Integer userId) {
    Friend friend = friendRepository.findByUserId(userId).orElse(null);
    List<Integer> friendIds = new ArrayList<>();
    if (friend != null) {
      friend.getFriends().forEach(f -> friendIds.add(f.getUserId()));
    }
    return friendIds;
  }
}
