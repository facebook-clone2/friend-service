// package com.example.friendservice.controller;
//
// import com.example.friendservice.model.FriendsModel;
// import com.example.friendservice.model.PostModel;
// import com.example.friendservice.service.FriendService;
// import lombok.extern.slf4j.Slf4j;
// import org.springframework.beans.factory.annotation.Autowired;
// import org.springframework.web.bind.annotation.*;
//
// import java.util.List;
//
// @Slf4j
// @RestController
// @CrossOrigin
// @RequestMapping("/friends")
// public class FriendController {
//
//    @Autowired
//    private FriendService friendService;
//
//    @PostMapping("/addfriend/{userId1}/{userId2}")
//    public String addFriend(@PathVariable Integer userId1, @PathVariable Integer userId2) {
//        log.info("Inside add friend: {}, {}", userId1, userId2);
//        if(friendService.addFriend(userId1, userId2))
//        return "Friends added successfully!";else
//            return "Friends Already!";
//    }
//
//    @GetMapping("/getfriends/{userId}")
//    public List<FriendsModel> getFriendsOfUser(@PathVariable Integer userId) {
//        return friendService.getFriends(userId);
//    }
//
//    @GetMapping("/getPostFromFriends/{userId}")
//    public List<PostModel> getPostsOfFriends(@PathVariable Integer userId){
//        return friendService.getPostsOfFriends(userId);
//    }
// }
package com.example.friendservice.controller;

import com.example.friendservice.entity.FriendRequest;
import com.example.friendservice.model.FriendsModel;
import com.example.friendservice.model.PostModel;
import com.example.friendservice.model.UserModel;
import com.example.friendservice.service.FriendService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.relational.core.sql.In;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
@CrossOrigin
@RequestMapping("/friends")
public class FriendController {

  @Autowired private FriendService friendService;

  @PostMapping("/addfriend/{userId1}/{userId2}")
  public String addFriend(@PathVariable Integer userId1, @PathVariable Integer userId2) {
    log.info("Inside add friend: {}, {}", userId1, userId2);
    return friendService.addFriendRequest(userId1, userId2);
  }

  @GetMapping("/getFriendRequests/{userId}")
  public List<UserModel> getFriendRequestDetails(@PathVariable Integer userId) {
    return friendService.getFriendRequestsByReceiver(userId, "PENDING");
  }

  @PostMapping("/approveFriendRequest/{senderUserId}/{receiverUserId}")
  public String approveFriendRequest(
      @PathVariable Integer senderUserId, @PathVariable Integer receiverUserId) {
    if (friendService.approveFriendRequest(senderUserId, receiverUserId)) {
      return "Friend request approved successfully!";
    } else {
      return "Friend request approval failed!";
    }
  }

  @GetMapping("/getfriends/{userId}")
  public List<FriendsModel> getFriendsOfUser(@PathVariable Integer userId) {
    return friendService.getFriends(userId);
  }

  @PostMapping("/getFriendsToNotify")
  public List<Integer> getFriendsList(@RequestBody Integer userId) {
    return friendService.getFriendsList(userId);
  }

  @GetMapping("/getPostFromFriends/{userId}")
  public List<PostModel> getPostsOfFriends(@PathVariable Integer userId) {
    return friendService.getPostsOfFriends(userId);
  }

  @GetMapping("/getStoryFromFriends/{userId}")
  public List<PostModel> getStoriesOfFriends(@PathVariable Integer userId) {
    return friendService.getStoriesOfFriends(userId);
  }

  @PostMapping("/areFriends")
  public Boolean areFriends(@RequestBody List<Integer> pair){
      return friendService.areFriends(pair);
  }
  @PostMapping("/declineFriendRequest/{senderUserId}/{receiverUserId}")
  public String declineFriendRequest(
      @PathVariable Integer senderUserId, @PathVariable Integer receiverUserId) {
    if (friendService.declineFriendRequest(senderUserId, receiverUserId)) {
      return "Friend request declined successfully!";
    } else {
      return "Friend request decline failed!";
    }
  }
}
