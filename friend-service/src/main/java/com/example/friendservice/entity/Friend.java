package com.example.friendservice.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.springframework.data.neo4j.core.schema.*;

import java.util.ArrayList;
import java.util.List;

@Node("Friend")
@ToString(exclude = {"friends"})
@EqualsAndHashCode
public class Friend {

  @Id @GeneratedValue private Long id;

  @Property(name = "userId")
  private Integer userId;

  @Relationship(type = "FRIEND")
  @JsonIgnore
  private List<Friend> friends;

  public List<Friend> getFriends() {
    if (friends == null) {
      friends = new ArrayList<>();
    }
    return friends;
  }

  public Integer getUserId() {
    return userId;
  }

  public void setUserId(Integer userId) {
    this.userId = userId;
  }
}
