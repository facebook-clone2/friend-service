package com.example.friendservice.entity;

import jakarta.persistence.*;
import jdk.jfr.Name;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.relational.core.mapping.Table;

@EqualsAndHashCode
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Slf4j
@Entity
@Table("friend_requests")
public class FriendRequest {
  @Id
  @Column(name = "id")
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @Column(name = "sentBy")
  private Integer senderUserId;

  @Column(name = "sentTo")
  private Integer receiverUserId;

  @Column(name = "Status")
  private String status; // Pending, Accepted, Declined

  @Column(name = "Time")
  private Long timestamp;
}
